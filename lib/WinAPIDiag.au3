#include-once

#include "APIDiagConstants.au3"
#include "StringConstants.au3"
#include "WinAPI.au3"
#include "WinAPIFiles.au3"
#include "WinAPIInternals.au3"
#include "WinAPIProc.au3"
#include "WinAPIShellEx.au3"
#include "WinAPITheme.au3"

; #INDEX# =======================================================================================================================
; Title .........: WinAPI Extended UDF Library for AutoIt3
; AutoIt Version : 3.3.14.2
; Description ...: Additional variables, constants and functions for the WinAPIDiag.au3
; Author(s) .....: Yashied, jpm
; ===============================================================================================================================

;;; cut content

; #FUNCTION# ====================================================================================================================
; Author.........: Yashied
; Modified.......: jpm
; ===============================================================================================================================
Func _WinAPI_IsInternetConnected()
	If Not __DLL('connect.dll') Then Return SetError(103, 0, 0)

	Local $aRet = DllCall('connect.dll', 'long', 'IsInternetConnected')
	If @error Then Return SetError(@error, @extended, 0)
	If Not ($aRet[0] = 0 Or $aRet[0] = 1) Then ; not S_OK nor S_FALSE
		Return SetError(10, $aRet[0], False)
	EndIf

	Return Not $aRet[0]
EndFunc   ;==>_WinAPI_IsInternetConnected

;;; cut content

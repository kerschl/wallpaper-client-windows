# Wallpaper Client Windows

This AutoIT script downloads picture and sets it as wallpaper.

dependencies: [imagemagick](https://imagemagick.org/script/download.php#windows)

Regedit paths:

'HKCU\SOFTWARE\Microsoft\Windows\CurrentVersion\Explorer\Wallpapers'
'HKCU\Control Panel\Desktop'

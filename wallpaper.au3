#pragma compile(Icon, favicon.ico)
#pragma compile(Out, wallpaper.exe)
#pragma compile(FileDescription, 'Webcam Wallpaper')
#pragma compile(FileVersion, 1.3)
#pragma compile(ProductName, Webcam Wallpaper)
#pragma compile(ProductVersion, 1.3)
#pragma compile(LegalCopyright, '@ Peter Kerschl')
#include <Inet.au3>
#include <WinAPIFiles.au3>
#include <lib/Json.au3>
#include <lib/WinAPIDiag.au3>

If Not _WinAPI_IsInternetConnected() Then
    Exit
Else
    
EndIf

$configFilePath = @AppDataDir & "\wallpaper-config.json"
If NOT FileExists($configFilePath) Then
    $configFileHandle = FileOpen($configFilePath, 2)
    If $configFileHandle = -1 Then
        MsgBox(16, "Error", "Unable to create the file.")
    Else
		$standardConfig = '{"config":{"wallpaper":"kampenwand","coordinates":"+150+80","wasserstand": false,"wassertemperatur": false,"options":["kampenwand","hochries-ost","tum-olympiapark","bad-endorf","rosenheim","chiemsee","prienavera","bernau-felden","deutschesmuseum2","domberg1","hochfelln","steinlingalm1","stoettham"]}}'
        FileWrite($configFileHandle, $standardConfig)
        FileClose($configFileHandle)
        MsgBox(0, "Config File Created", "A standard config file was created.")
    EndIf
EndIf

$configFileLink = @AppDataDir & "\Microsoft\Windows\Start Menu\Programs\Open Wallpaper Config.lnk"
If NOT FileExists($configFileLink) Then
	FileCreateShortcut($configFilePath, $configFileLink)
EndIf

$configRaw = FileRead($configFilePath)
$configJson = Json_Decode($configRaw)
$location = Json_ObjGet($configJson, 'config.wallpaper')
$coordinates = Json_ObjGet($configJson, 'config.coordinates')
$sleep = Json_ObjGet($configJson, 'config.sleep')

$url = "https://pmke.de/api/wallpaper/" & $location
$responseRaw = _INetGetSource($url)
$responseJson = Json_Decode($responseRaw)
$urlcurrent = Json_ObjGet($responseJson, 'data.current')
$datetime = Json_ObjGet($responseJson, 'data.datetime')
$imagepath = @LocalAppDataDir & "\wallpaper.jpg"
InetGet($urlcurrent, $imagepath)

$textOnWallpaper = $location & ': ' & $datetime

If (Json_ObjGet($configJson, 'config.wasserstand')) Then
    $url = "https://pmke.de/api/wasserstand/prien.json"
    $responseRaw = _INetGetSource($url)
    $responseJson = Json_Decode($responseRaw)
    $wasserstand = Json_ObjGet($responseJson, 'data.current')
    $textOnWallpaper = $textOnWallpaper & ', wasserstand: ' & $wasserstand & 'm ü. NN'
EndIf

If (Json_ObjGet($configJson, 'config.wassertemperatur')) Then
    $url = "https://pmke.de/api/wassertemperatur/prien.json"
    $responseRaw = _INetGetSource($url)
    $responseJson = Json_Decode($responseRaw)
    $wasserstand = Json_ObjGet($responseJson, 'data.current')
    $textOnWallpaper = $textOnWallpaper & ', wassertemperatur: ' & $wasserstand & '°C'
EndIf

ShellExecuteWait( "magick.exe", $imagepath & ' -pointsize 42 -fill black -annotate ' & $coordinates & ' "' & $textOnWallpaper & '" ' & $imagepath, "", "open", @SW_HIDE)
RegWrite('HKCU\Control Panel\Desktop', 'TileWallpaper', 'reg_sz', '0')
RegWrite('HKCU\Control Panel\Desktop', 'WallpaperStyle', 'reg_sz', '10')
RegWrite('HKCU\Control Panel\Desktop', 'Wallpaper', 'reg_sz', $imagepath)
DllCall("User32.dll", "int", "SystemParametersInfo", "int", 20, "int", 0, "str", $imagepath, "int", 0)
